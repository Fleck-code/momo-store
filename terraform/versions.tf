terraform {
  required_version = ">= 1.0.0"
  backend "s3" {
    endpoint   = "storage.yandexcloud.net"
    bucket     = "rnandrei"
    region     = "ru-central1"
    key        = "terraform.tfstate"

    skip_region_validation      = true
    skip_credentials_validation = true
    skip_metadata_api_check     = true

    dynamodb_endpoint = "https://docapi.serverless.yandexcloud.net/ru-central1/b1gsdfm7r9vu8f1h36no/etnovger2e1uli3rshmi"
    dynamodb_table = "tfstate"
} 
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">= 0.87.0"
    }
  }
}
