output "cluster_id" {
  value = yandex_kubernetes_cluster.k8s-zonal.id
}

output "subnet_id" {
  value = yandex_vpc_subnet.mysubnet.id
}

output "security_group_ids" {
  value = yandex_kubernetes_cluster.k8s-zonal.master[0].security_group_ids
}