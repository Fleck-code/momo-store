resource "yandex_kubernetes_node_group" "default" {
  cluster_id = yandex_kubernetes_cluster.k8s-zonal.id
  name       = "my-node-group"

  scale_policy {
    fixed_scale {
      size = 1
    }
  }

  deploy_policy {
    max_expansion   = 3
    max_unavailable = 1
  }

  instance_template {
    platform_id = "standard-v1"
    resources {
      memory = 4
      cores  = 2
    }

    boot_disk {
      size = 64
      type = "network-ssd"
    }

    network_interface {
      subnet_ids = [yandex_vpc_subnet.mysubnet.id]
      security_group_ids = yandex_kubernetes_cluster.k8s-zonal.master[0].security_group_ids
      nat = true
    }
  }
}
