# Momo Store aka Пельменная №2

<img width="900" alt="image" src="https://user-images.githubusercontent.com/9394918/167876466-2c530828-d658-4efe-9064-825626cc6db5.png">

## Устройство репозитория


- frontend - исходный код fronend
- backend - исходный код backend
- helm - helm чарт для разворачивания приложений в кластере kubernetes
- terraform - terraform манифесты для разворачивания кластера kubernetes и группы нод в yandex cloud, а также gitlab ci

## Frontend local build

```bash
cd frontend
npm install
NODE_ENV=production VUE_APP_API_URL=http://localhost:8081 npm run serve
```

## Backend local build

```bash
cd  backend
go run ./cmd/api
```

## Infrastructure

terraform/.gitlab-ci.yml - для автоматизации поднятия и уничтожения инфраструктуры в качестве managed kubernetes кластер в yc и одной ноды.

Поднимается ingress-контроллер и две среды для разработки DEV без tls, и PROD с tls

## Пельменная №2 в kubernetes

Следующая команда поднимет по два пода backend и frontend в кластере kubernetes:

```bash
helm upgrade --atomic --install momo-store helm
```
Версии приложения меняются в helm/Chart.yaml

Переменные меняются в helm/values.yaml

Новые версии приложения могут применяться командой выше

## CI/CD

Pipelines организованы как Downstream pipelines для удобства и разделения сборок двух приложений и поднятия инфраструктуры с помощью Terraform.

В каждой директории fronend, backend и terraform есть child-pipelines для двух приложений и поднятия инфраструктуры со своей сборкой.

Тест обоих приложений производится с помощью встроенных тестов GitLab SAST, а также внешним сервисом Sonarqube.

После проверки тестами производится релиз докер образов в Gitlab registry.

Deploy DEV или PRODUCTION сред производится вручную по кнопке.

Destroy производится тоже вручную по кнопке.

## Variables

AWS_ACCESS_KEY_ID - ID сервисного аккаунта для хранения .tfstate в S3 yc и dynamoDB

AWS_SECRET_ACCESS_KEY - secret key cервисного аккаунта для хранения .tfstate в S3 yc и dynamoDB

DEV_DOMAIN_NAME - адрес dev сайта

DOCKERSECRET - доступ в docker репозиторий

DOMAIN_NAME - адрес prod сайта

SONARQUBE_URL - адрес Sonarqube

SONAR_PROJECT_KEY_BACK - имя проекта с backend

SONAR_PROJECT_KEY_FRONT - имя проекта с frontend

SONAR_TOKEN - для доступа в sonarqube

YC_CLOUD_ID - cloud_id yandex cloud

YC_FOLDER_ID - folder_id yandex cloud

YC_KEY - ключ авторизации

ca_pem - kubernetes certificate for kubectl auth

tls_crt - tls certificate

tls_key - tls private key

## To improve

- На текущий момент настройки DNS необходимо править вручную. После создания Ingress-controller необходимо ip адрес load balancer добавить как А запись в свой DNS.

- Добавить мониторинг и логирование

## Документация и подготовительные шаги:

[Начало работы с интерфейсом командной строки](https://yandex.cloud/ru/docs/cli/quickstart#install)

[Начало работы с Terraform](https://yandex.cloud/ru/docs/tutorials/infrastructure-management/terraform-quickstart#install-terraform)

[Создание кластера Managed Service for Kubernetes](https://yandex.cloud/ru/docs/managed-kubernetes/operations/kubernetes-cluster/kubernetes-cluster-create)

[Создание группы узлов](https://yandex.cloud/ru/docs/managed-kubernetes/operations/node-group/node-group-create)

Как получить KUBECONFIG: [Создание статического файла конфигурации](https://yandex.cloud/ru/docs/managed-kubernetes/operations/connect/create-static-conf)

[Установка Ingress-контроллера NGINX с сертификатом из Certificate Manager](https://yandex.cloud/ru/docs/certificate-manager/tutorials/nginx-ingress-certificate-manager)
